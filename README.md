# TrueMark System Images

This project is used by TrueMark to generate ISO images, VMware images 
and AWS AMIs used by TrueMark as a base for it's hosted and managed
systems.

Be aware this project was setup to be run on macOs. You may run into
issues if you attempt to run this on a different operating system.

## ISO Builds

### Pre-Requisites
1. Docker Engine must be installed
2. If you plan to build customized Oracle Linux ISOs you must download the ISO from Oracle.

### Create Ubuntu 18.04 ISO
```bash
./isos/run.sh ubuntu-18.04
```

### Create Ubuntu 20.04 ISO
```bash
./isos/run.sh ubuntu-20.04
```

### Create Oracle Linux 7 ISO
Make sure you have downloaded the Oracle Linux 7 ISO to the oracle-linux-7 directory.
```bash
./isos/run.sh oracle-linux-7
```

## Basic Builds

### Pre-Requisites
1. HashiCorp Packer must be installed
2. If you plan to create VMware VMs you must have VMware Fusion installed.

### Create Ubuntu 18.04 Basic Image
```
./basic/run.sh ubuntu-18.04-vmware
```

### Create Ubuntu 20.04 Basic Image
```
./basic/run.sh ubuntu-20.04-vmware
```
µ
### Create Oracle Linux 7 Basic Image
```
./basic/run.sh oracle-linux-7-vmware
```

Once that's done you can export the VM from VMware Fusion to an OVA file.


