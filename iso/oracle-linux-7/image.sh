#!/usr/bin/env bash

set -ueo pipefail
ISOFILE="V995537-01.iso"
ISODIR="/tmp/iso"
NEWISO="truemark-oracle-linux-7.iso"
SCRIPTDIR="$(dirname "${0}")"

cd "${SCRIPTDIR}" || exit 1
echo "Running from $(pwd)"

[[ -f "${ISOFILE}" ]] ||  (echo "${ISOFILE} is missing" && exit 1)

echo "Extracting ISO"
osirrox -indev "$(pwd)/${ISOFILE}" -extract / "${ISODIR}"

echo "Customizing ISO"
set -x

find "${ISODIR}" -type d -exec chmod 755 {} \;
find "${ISODIR}" -type f -exec chmod 644 {} \;

cp minimum-ks.cfg "${ISODIR}/ks.cfg"
sed -i 's/timeout 600/timeout 1/' "${ISODIR}/isolinux/isolinux.cfg"
sed -i 's/  append initrd=initrd.img inst.stage2=hd:LABEL=OL-7.* rd.live.check quiet/  append initrd=initrd.img ks=cdrom:\/ks.cfg inst.cmdline quiet/' "${ISODIR}/isolinux/isolinux.cfg"

mkisofs -r -V "Oracle Linux Install CD" \
  -cache-inodes \
  -J -l -b isolinux/isolinux.bin \
  -c isolinux/boot.cat -no-emul-boot \
  -boot-load-size 4 -boot-info-table \
  -o "${NEWISO}" "${ISODIR}"

sha256sum "${NEWISO}" | tee "${NEWISO}.sha256"

set +x
echo "Completed"
