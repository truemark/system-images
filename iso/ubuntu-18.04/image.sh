#!/usr/bin/env bash

set -ueo pipefail
ISOFILE="ubuntu-18.04.5-server-amd64.iso"
ISOURL="http://cdimage.ubuntu.com/releases/18.04.5/release/${ISOFILE}"
ISODIR="/tmp/iso"
NEWISO="truemark-ubuntu-18.04.iso"
SCRIPTDIR="$(dirname "${0}")"

cd "${SCRIPTDIR}" || exit 1
echo "Running from $(pwd)"

echo "Downloading ISO"
if [[ ! -f "${ISOFILE}" ]]; then
  curl -C - -SO "${ISOURL}"
fi

echo "Extracting ISO"
osirrox -indev "$(pwd)/${ISOFILE}" -extract / "${ISODIR}"

echo "Customizing ISO"
set -x

find "${ISODIR}" -type d -exec chmod 755 {} \;
find "${ISODIR}" -type f -exec chmod 644 {} \;

cp truemark.seed "${ISODIR}/preseed"
sed -i 's/file=\/cdrom\/preseed\/ubuntu-server.seed vga=788 initrd=\/install\/initrd.gz/auto=true priority=critical file=\/cdrom\/preseed\/truemark.seed vga=788 initrd=\/install\/initrd.gz/' "${ISODIR}/isolinux/txt.cfg"
sed -i 's/timeout .*/timeout 1/' "${ISODIR}/isolinux/isolinux.cfg"

mkisofs -r -V "TrueMark Ubuntu Install CD" \
  -cache-inodes \
  -J -l -b isolinux/isolinux.bin \
  -c isolinux/boot.cat -no-emul-boot \
  -boot-load-size 4 -boot-info-table \
  -o "${NEWISO}" "${ISODIR}"

sha256sum "${NEWISO}" | tee "${NEWISO}.sha256"

set +x
echo "Completed"
