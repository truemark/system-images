#!/usr/bin/env bash

set -euo pipefail

[[ -n "${1+z}" ]] || (echo "Script required as first argument" && exit 1)

DOCKER_ARGS="" && [[ -t 1 ]] && DOCKER_ARGS="-it"

cd "$(dirname "${0}")" || exit 1

docker build -t isobuilder .

docker run ${DOCKER_ARGS} --rm \
  -v "$(pwd)/${1}:/live" \
  isobuilder "/live/image.sh"

scp -P 2020 ${1}/*.iso download@69.160.74.206:iso/
scp -P 2020 ${1}/*.iso.sha256 download@69.160.74.206:iso/
