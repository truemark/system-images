#!/usr/bin/env bash

set -euo pipefail

[[ -n "${1+z}" ]] || (echo "File required as first argument" && exit 1)

cd "$(dirname "${0}")" || exit 1

[[ -e "vars.hcl" ]] || (echo "vars.hcl is missing" && exit 1)

# Cleanup
rm -rf packer_cache output-basic *.ova *.box *.sha256

# Create VM
packer build -var-file=vars.hcl "${1}"
