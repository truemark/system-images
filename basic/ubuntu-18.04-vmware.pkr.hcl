variable "vagrant_access_token" {}

# https://www.packer.io/docs/builders/vmware/iso
source "vmware-iso" "basic" {
  #iso_url = "http://download.truemark.io/iso/truemark-ubuntu-18.04.iso"
  #iso_checksum = "file:http://download.truemark.io/iso/truemark-ubuntu-18.04.iso.sha256"
  iso_url = "../iso/ubuntu-18.04/truemark-ubuntu-18.04.iso"
  iso_checksum = "file:../iso/ubuntu-18.04/truemark-ubuntu-18.04.iso.sha256"
  ssh_username = "user"
  ssh_password = "truemark"
  shutdown_command = "sudo shutdown -h now"
  # https://kb.vmware.com/s/article/1003746
  version = "13" # ESXi 6.5
  vm_name = "TrueMark Ubuntu 18.04 Basic"
  # https://github.com/josenk/vagrant-vmware-esxi/wiki/VMware-ESXi-6.5-guestOS-types
  guest_os_type = "ubuntu-64"
  cpus = 1
  memory = 1024
  disk_adapter_type = "pvscsi"
  disk_size = 102400
  disk_type_id = 0
  network_adapter_type = "vmxnet3"
}

build {
  sources = ["sources.vmware-iso.basic"]

  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; {{ .Vars }} sudo -S -E bash -c '{{.Path}}'"
    scripts = [
      "scripts/update.sh",
      "scripts/ubuntu-18.04.sh",
      "scripts/ssh.sh",
      "scripts/vmware.sh",
      "scripts/cleanup.sh"
    ]
  }

  post-processors {
    post-processor "shell-local" {
      inline = ["./export-ova.sh 'TrueMark Ubuntu 18.04 Basic'"]
    }
  }
}
