#!/usr/bin/env bash

set -eou pipefail

[[ -n "${1}" ]] || (echo "First argument must be VM Name" || exit 1)

/Applications/VMware\ Fusion.app/Contents/Library/VMware\ OVF\ Tool/ovftool \
  --acceptAllEulas "./output-basic/${1}.vmx" \
  "${1}.ova"
shasum -a 256 "${1}.ova" | tee "${1}.ova.sha256"

scp -P 2020 *.ova download@69.160.74.206:vmware-templates/
scp -P 2020 *.sha256 download@69.160.74.206:vmware-templates/
