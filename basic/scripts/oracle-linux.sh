#!/usr/bin/env bash

set -euo pipefail

echo "###############################################################################"
echo "Running oracle-linux.sh - ${0}"
echo "###############################################################################"

yum install -y -q openssh-server open-vm-tools perl ethtool
