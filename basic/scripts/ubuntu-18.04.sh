#!/usr/bin/env bash

set -euo pipefail
export DEBIAN_FRONTEND=noninteractive

echo "###############################################################################"
echo "Running ubuntu-18.04.sh - ${0}"
echo "###############################################################################"

#------------------------------------------------------------------------------
# Dependencies
#------------------------------------------------------------------------------
apt-get -qq install openssh-server open-vm-tools perl-modules-5.26 ethtool
