#!/usr/bin/env bash

set -euo pipefail
export DEBIAN_FRONTEND=noninteractive

echo "###############################################################################"
echo "Running vmware.sh - ${0}"
echo "###############################################################################"

#------------------------------------------------------------------------------
# VMware Fails on missing /etc/dhcp/dhcp3
# https://kb.vmware.com/s/article/2051469
#------------------------------------------------------------------------------
if [[ -d "/etc/dhcp" ]]; then
  if [[ ! -d "/etc/dhcp3" ]]; then
    ln -s /etc/dhcp /etc/dhcp3
  fi
fi

#------------------------------------------------------------------------------
# Increase network buffers VMware
#------------------------------------------------------------------------------
cat <<-EOF >> /etc/rc.local

# Set buffers on ethernet devices
for i in $(ip -br link show  | grep -v LOOP | awk '{print $1}');
  do /sbin/ethtool -G \$i rx 4096 tx 4096 | true;
done

EOF
