#!/usr/bin/env bash

set -euo pipefail
export DEBIAN_FRONTEND=noninteractive
export TERM=linux

echo "###############################################################################"
echo "Running update.sh - ${0}"
echo "###############################################################################"

#------------------------------------------------------------------------------
# OS Updates
#------------------------------------------------------------------------------
if command -v apt-get &> /dev/null; then
  sudo apt-get -qq update
  sudo apt-get -qq upgrade
fi

if command -v yum &> /dev/null; then
  sudo yum update -y -q
fi
