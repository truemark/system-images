#!/usr/bin/env bash

set -euo pipefail
export DEBIAN_FRONTEND=noninteractive

echo "###############################################################################"
echo "Running ubuntu-20.04.sh - ${0}"
echo "###############################################################################"

#------------------------------------------------------------------------------
# Dependencies
#------------------------------------------------------------------------------
apt-get -qq install openssh-server open-vm-tools perl-modules-5.30 ethtool
