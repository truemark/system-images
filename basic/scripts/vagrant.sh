#!/usr/bin/env bash

set -euo pipefail
export DEBIAN_FRONTEND=noninteractive

echo "###############################################################################"
echo "Running vagrant.sh - ${0}"
echo "###############################################################################"

#------------------------------------------------------------------------------
# Vagrant Specific Settings
#------------------------------------------------------------------------------
groupadd vagrant
useradd -g vagrant -m -s /bin/bash vagrant
mkdir -p /home/vagrant/.ssh
chmod 700 /home/vagrant/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant insecure public key" > /home/vagrant/.ssh/authorized_keys
chmod 600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant:vagrant /home/vagrant/.ssh
echo -n vagrant chpasswd vagrant
echo -n vagrant chpasswd root
echo 'vagrant ALL=(ALL) NOPASSWD:ALL' >/etc/sudoers.d/vagrant;
chmod 440 /etc/sudoers.d/vagrant;

if [[ -d "/etc/netplan" ]]; then
  echo "Create netplan config for eth0"
  cat <<EOF >/etc/netplan/01-netcfg.yaml;
network:
  version: 2
  ethernets:
    eth0:
      dhcp4: true
EOF
fi

if [[ -d "/etc/sysconfig/network-scripts" ]]; then
  echo "Creating ifcfg-eth0"
  rm -f /etc/sysconfig/network-scripts/ifcfg-e*
  cat <<EOF > /etc/sysconfig/network-scripts/ifcfg-eth0
TYPE="Ethernet"
BOOTPROTO="dhcp"
NAME="eth0"
DEVICE="eth0"
ONBOOT="yes"
IPV6INIT="no"
EOF
fi

if command -v apt-get &> /dev/null; then
  # Install fuse for HGFS shared folders
  apt-get install fuse -y
fi
mkdir -p /mnt/hgfs

# Disable predictable network interface names
sed -i 's/GRUB_CMDLINE_LINUX="\(.*\)"/GRUB_CMDLINE_LINUX="net.ifnames=0 biosdevname=0 \1"/g' /etc/default/grub
if command -v update-grub &> /dev/null; then
  update-grub
fi
if command -v grub2-mkconfig &> /dev/null; then
  grub2-mkconfig -o /boot/grub2/grub.cfg
fi

# Speed up SSH
sed -i 's/#UseDNS yes/UseDNS no/g' /etc/ssh/sshd_config
sed -i 's/GSSAPIAuthentication yes/GSSAPIAuthentication no/g' /etc/ssh/sshd_config
