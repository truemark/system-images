#!/usr/bin/env bash

set -euo pipefail
export DEBIAN_FRONTEND=noninteractive

echo "###############################################################################"
echo "Running ssh.sh - ${0}"
echo "###############################################################################"

#------------------------------------------------------------------------------
# SSH Host Keys
#------------------------------------------------------------------------------
# Remove host keys since this is a template
rm -f /etc/ssh/*_key*

[[ -f "/etc/rc.local" ]] || echo '#!/bin/sh' > /etc/rc.local
chmod +x /etc/rc.local

cat <<-EOF >> /etc/rc.local

# Generate SSH Host Keys if Missing
[ \$(ls -1 /etc/ssh/*_key* 2> /dev/null| wc -l) -ne 0 ] || ssh-keygen -A

EOF
